# Offerings

Offerings is a Django app to offer products and services.

## Quick start

1. Add "offerings" to your INSTALLED_APPS setting like this::
    ```python
    INSTALLED_APPS = [
        # ...
        'offerings',
    ]
    ```

2. Include the offerings URLconf in your project urls.py like this::

    path('offerings/', include('offering.urls')),

3. Run ``python manage.py migrate`` to create the offerings models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create an offering (you'll need the Admin app enabled).