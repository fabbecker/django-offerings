from django.contrib import admin

from .models import Collection, Offer


admin.site.register(Collection)
admin.site.register(Offer)
