from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Collection(models.Model):
    """
    A collection of offers
    """
    provided_by = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=128, primary_key=True)
    description = models.TextField(help_text="Describe your collection")

    def __str__(self):
        return f"{self.title} by {self.provided_by}"

    def get_offers(self, include_unavailable: bool = False):
        if include_unavailable:
            offers = Offer.objects.filter(
                collection=self,
            )
        else:
            offers = Offer.objects.filter(
                collection=self,
                available_from__lte=timezone.now(),
                available_until__gte=timezone.now(),
            )
        return offers


class Offer(models.Model):
    """
    An offer, that is made to the public
    """
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    title = models.CharField(
        max_length=256,
        primary_key=True,
        )
    description = models.TextField(
        blank=True,
        )
    available_from = models.DateTimeField(
        blank=True,
        default=timezone.now,
        )
    available_until = models.DateTimeField(
        blank=True,
        default=timezone.now,
        )

    def __str__(self):
        return f"{self.title} by {self.collection.provided_by}"
