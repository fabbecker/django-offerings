from django.http import Http404
from django.utils import timezone
from django.views import generic

from .models import Collection
from .models import Offer


class CollectionView(generic.DetailView):
    model = Collection
    template_name = 'collection.html'

    def get_context_data(self, **kwargs):
        context = super(CollectionView, self).get_context_data(**kwargs)
        context['offers'] = self.object.get_offers(
            include_unavailable=self.object.provided_by == self.request.user,
        )
        return context


class OfferView(generic.DetailView):
    model = Offer
    template_name = 'offer.html'

    def get_context_data(self, **kwargs):
        context = super(OfferView, self).get_context_data(**kwargs)
        offer = context['offer']
        user = self.request.user
        if offer.available_from < timezone.now() < offer.available_until or offer.collection.provided_by == user:
            return context
        else:
            raise Http404("Offer not found.")
