import datetime

from django.http import Http404
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone

from .models import Collection, Offer, User


class OfferModelTests(TestCase):
    def test_offers_in_collection_and_offer_view(self):
        client = Client()
        user = User(username='user')
        user.save()
        collection = Collection(title='coll', provided_by=user)
        collection.save()

        offer_not_available_yet = Offer(
            collection=collection, title='Not available yet',
            available_from=timezone.now() + datetime.timedelta(seconds=10),
            )
        offer_not_available_yet.save()
        # Check collection view
        url_collection = reverse('collection', kwargs={'username': user.username, 'pk': collection.title})
        response = client.get(url_collection)
        self.assertNotIn(
            offer_not_available_yet,
            response.context_data['offers'],
            'Offer is available too early'
            )
        # Check offer view
        url_offer = reverse(
            'offer',
            kwargs={'username': user.username, 'collection': collection.title, 'pk': offer_not_available_yet.title}
            )
        response = client.get(url_offer)
        self.assertEqual(
            response.status_code,
            404,
            "Unavailable message is already shown before its release date",
            )

        offer_available = Offer(
            collection=collection, title='This offer is available',
            available_from=timezone.now() - datetime.timedelta(seconds=10),
            available_until=timezone.now() + datetime.timedelta(seconds=10),
            )
        offer_available.save()
        # Check collection view
        response = client.get(url_collection)
        self.assertIn(
            offer_available,
            response.context_data['offers'],
            'Offer is not available when it should be'
            )
        # Check offer view
        url_offer = reverse(
            'offer',
            kwargs={'username': user.username, 'collection': collection.title, 'pk': offer_available.title}
            )
        response = client.get(url_offer)
        self.assertEqual(
            response.context_data['offer'],
            offer_available,
            "Unavailable message is not shown when it is supposed to be",
            )

        offer_not_available_anymore = Offer(
            collection=collection, title='Not available yet',
            available_from=timezone.now() + datetime.timedelta(seconds=10),
            )
        offer_available.save()
        # Check collection view
        response = client.get(url_collection)
        self.assertNotIn(
            offer_not_available_anymore,
            response.context_data['offers'],
            'Offer is available when it should not be yet',
            )
        # Check offer view
        url_offer = reverse(
            'offer',
            kwargs={'username': user.username, 'collection': collection.title, 'pk': offer_not_available_anymore.title}
            )
        response = client.get(url_offer)
        self.assertEqual(
            response.status_code,
            404,
            "Unavailable message is still shown after its release date",
            )
